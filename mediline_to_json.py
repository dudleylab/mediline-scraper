"""Scrape sections from the mediline encyclopedia.

Use the scraper module to scrape set sections from all pages on then
mediline medical encyclopedia and store them as JSON files.
"""
from json import dump, load
from string import ascii_uppercase
from textwrap import dedent
import argparse
from itertools import groupby
from operator import itemgetter

from scraper import (encyclopedia_page,
                     linked_pages,
                     sections_bullets,
                     title)  # noqa


# sections to scrape on each topic page
SECTIONS = ["Symptoms",
            "Exams and Tests",
            "Treatment",
            "Outlook (Prognosis)"]


def scrape_letter(letter, topics):
    """A subroutine to scrape a single letter."""
    page = encyclopedia_page(letter)
    # The dictionary that will be written to the file as a JSON object.
    letter_dict = {}

    # Scrape the given topics from the page and set the progress bar settings.
    topics = linked_pages(page,
                          topics=topics,
                          progress_bar=True,
                          title="Fetching '{}' topics".format(letter))

    print("Building dictionary")

    for topic in topics:
        # Get the bullets for each section and add it to the
        # dictionary.
        topic_name = title(topic)
        letter_dict[topic_name] = sections_bullets(topic, SECTIONS)

    return letter_dict


def scrape(topic_dict, json_dir, update):
    """Fetch sections from all topics in all letters."""
    for letter, topics in topic_dict.items():
        # Create a new JSON file.
        filename = json_dir + 'mediline_{}.json'.format(letter)
        with open(filename, 'r+' if update else 'w') as json_file:
            letter_dict = scrape_letter(letter, topics)

            print("Writing JSON to '{}'\n".format(filename))

            if update:
                # Python 2.7 uses in-place dict updates; we create a temp dict.
                old_dict = load(json_file)
                old_dict.update(letter_dict)
                letter_dict = old_dict

            # Because we need to read the file to get the original JSON
            # out of it, we now need to simulate the behavior of the 'w'
            # file mode by going to the beggining, writing the JSON data
            # then truncating the file.
            json_file.seek(0)
            dump(letter_dict, json_file)
            json_file.truncate()

    completion_message = dedent("""\
                                Done scraping mediline encyclopedia.
                                The resulting JSON files are located in '{}'.
                                """.format(json_dir))

    print(completion_message)

if __name__ == '__main__':
    desc = "Scrape data from the mediline medical encyclopedia."
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('directory',
                        metavar='dir',
                        type=str,
                        default='./JSON/',
                        help='directory to write JSON files to')
    parser.add_argument('-f',
                        '--file',
                        type=file,
                        default=None,
                        help="file to read topics from")
    help_msg = 'flag to update JSON files in directory instead of writing' + \
               'new ones'
    parser.add_argument('-u',
                        '--update',
                        dest='update',
                        action='store_true',
                        help=help_msg)
    args = parser.parse_args()

    print(args)

    # The user supplied the optional disease list
    if args.file is not None:
        topics = []
        with args.file as topic_file:
            for line in topic_file.readlines():
                topics.append(line.strip("'\n"))
        topics.sort()
        # Use the user supplied disease list to build the topic_dict
        topic_dict = {k: list(g) for k, g in groupby(topics, itemgetter(0))}
    else:
        # Otherwise, the topic_dict will have None for values, which the
        # scraper will interprate as everything.
        topic_dict = {letter: None for letter in ascii_uppercase}

    scrape(topic_dict, args.directory, args.update)
