"""The scraper module.

Abstracts the process of crawling the mediline medical encyclopedia and
scraping its html into JSON."""
from string import ascii_uppercase

from lxml import html
from pyprind import ProgBar
from requests import get

BASE_URL = "https://medlineplus.gov/ency/"


def _tree_for_url(url, **kwargs):
    """Helper method to get a Tree object from a given url."""
    return html.fromstring(get(url).content, **kwargs)


def encyclopedia_page(letter):
    """Get the Tree Object given page of the mediline encyclopedia.

    Given a letter, the function scrapes the html of the correct page of
    the mediline medical encyclopedia and returns a Tree representing
    its contents."""
    upper_letter = letter.upper()

    if upper_letter not in list(ascii_uppercase):
        raise ValueError("expected a single letter; got " + upper_letter)

    url = "{}encyclopedia_{}.htm".format(BASE_URL, upper_letter)
    return _tree_for_url(url, base_url=BASE_URL)


def _xpath_or(attr, *args):
    """Helper method to build xpath or clause from arg sequence."""
    s = '['
    s += ' or '.join('{}="{}"'.format(attr, arg) for arg in args)
    s += ']'
    return s


def linked_pages(tree, topics=None, progress_bar=False, **kwargs):
    """Get Tree objects for pages from a page of the mediline encyclopedia.

    Given a Tree object representing a lettered page of the mediline
    medical encyclopedia, the function parses out the absolute URLs for
    the listed pages and returns them as a list of strings.
    """
    # If we are passed a topic list for filtering purposes, we create
    # the filtering xpath clause. If not, then the clause is just the
    # null string.
    xpath_filter = '' if topics is None else _xpath_or('.', *topics)
    urls_path = '//ul[@id="index"]/li{}//@href'.format(xpath_filter)
    relative_urls = tree.xpath(urls_path)

    if progress_bar:
        bar = ProgBar(len(relative_urls), **kwargs)

    # Build the list of Tree objects for pages linked from this encyclopedia
    # letter.
    trees = []
    for url in relative_urls:
        tree = _tree_for_url(BASE_URL + url, base_url=BASE_URL)
        trees.append(tree)
        if progress_bar:
            bar.update()

    print(bar)

    return trees


def title(tree):
    """Get the title for a page from the mediline encyclopedia."""
    path = '''//div[@class="page-title"]
              /child::h1'''
    title = tree.xpath(path)[0].text

    return title


def section_bullets(tree, section):
    """Helper method for get_sections to get a single section."""
    # eg .="Symptoms" will pull out the h2 tag with the text "Symptoms"
    bullets_path = '''//h2[. = "{}"]
                      /../../following-sibling::div[1]
                      /ul/li'''.format(section)

    # Pulls the text from list item tags, even if some text is contained within
    # an anchor tag within the list item tag.
    bullet_text_path = '''./descendant-or-self::*
                           /text()'''

    bullets = tree.xpath(bullets_path)

    # Concatenate the text within each bullet (in case of aforementioned
    # anchor tags).
    return [''.join(bullet.xpath(bullet_text_path)) for bullet in bullets]


def sections_bullets(tree, sections):
    """Get content of given sections from article Tree object.

    Given a Tree object representing an article from the mediline
    medical encyclopedia, the function parses out the content of the
    sections identified by the given section names into a dictionary and
    with section names for keys and a list of the text of <li> tags as
    values, and returns it."""
    section_dict = {section: section_bullets(tree, section)
                    for section in sections}

    return section_dict
