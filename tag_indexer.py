"""Module to parse out tags from the medililine scraper data.

The module takes the scraped data as JSON and uses NLTK to extract useful
ngrams from the bullets as tags and writes them to separate JSON files.
"""
import csv
import json
from string import punctuation

import numpy as np
import nltk

UMLS_ROOT = 'scraper-dictionary/umls/'
STOP_WORDS_ROOT = 'scraper-dictionary/stop-words/'

ENGLISH_STOP = set()
MEDICAL_STOP = set()
PUNCTUATION = set(punctuation)
UMLS_TERMS = set()

with open(STOP_WORDS_ROOT + 'english.csv') as csv_file:
    for line in csv.reader(csv_file):
        ENGLISH_STOP.add(line[0])

with open(STOP_WORDS_ROOT + 'medical.csv') as csv_file:
    for line in csv.reader(csv_file):
        MEDICAL_STOP.add(line[0])

with open(UMLS_ROOT + 'orig-umls-dictionary.csv') as csv_file:
    for line in csv.reader(csv_file):
        # if line[-1] == "sign or symptom":
            UMLS_TERMS.add(line[0])
            UMLS_TERMS.add(line[1])

ALL_STOP = ENGLISH_STOP | MEDICAL_STOP | PUNCTUATION
BAD_POS = {
    'JJ',  # adjectives
    'RB',  # adverbs
    'DT',  # determiners
    'VB',  # verbs
}


def substringSieve(string_list):
    """Filter out substrings in the list.

    Given a list of strings, filter out any strings who are substrings
    of other strings in the list.
    """
    string_list.sort(key=lambda s: len(s), reverse=True)
    out = []
    for s in string_list:
        if (not any(s in o for o in out) and
            not any(p in s for p in punctuation)):
            out.append(s)
    return out


def stop_surround(ngram):
    """If there are stop words on the end of the gram, truncate them."""
    while len(ngram) > 1 and ngram[0, 0] in ENGLISH_STOP | PUNCTUATION:
        ngram = ngram[1:]
    while len(ngram) > 1 and ngram[-1, 0] in ENGLISH_STOP | PUNCTUATION:
        ngram = ngram[:-1]
    return ngram


def bad_pos_gram(ngram_pos):
    """Determine if the gram is invalid due to POS."""
    return (all(pos in BAD_POS for pos in ngram_pos) or
            ngram_pos[-1] in ("VB",
                              "VBP",
                              "VBZ",
                              "VBN",
                              "VBD",
                              "VBG",
                              "IN",
                              "TO",
                              "CC"))


def _stop_matches(ngram_tokens):
    return [token in ALL_STOP for token in ngram_tokens]


def all_stop(ngram_tokens):
    """Determine if all tokens in the gram are stop words."""
    return all(_stop_matches(ngram_tokens))


def partial_stop(ngram_tokens, stop_threshold):
    """Determine if some of the tokens in the gram are stop words.

    Determine if there are at least as many stop words in the gram as
    sepecified by the stop_threshold argument."""
    return _stop_matches(ngram_tokens).count(True) >= stop_threshold


def any_stop(ngram_tokens):
    """Determine if any of the tokens in the gram are stop words."""
    return any(_stop_matches(ngram_tokens))


def stop_ngram(ngram_tokens, stop_threshold="all"):
    """Call the appropriate stop words checker.

    Call the appropriate stop words validation method based on the value
    of the stop_threshold argument.
    """
    if stop_threshold == "all":
        return all_stop(ngram_tokens)
    if stop_threshold == "any":
        return any_stop(ngram_tokens)
    elif isinstance(stop_threshold, int):
        return partial_stop(ngram_tokens, stop_threshold)

    err_msg = "stop_threshold must be either 'all', 'any', or an int literal"
    raise ValueError(err_msg)


def junk_gram(tagged_ngram, stop_threshold="all"):
    """Determine if the gram is bad due to stop words or POS."""
    ngram_tokens, ngram_pos = tagged_ngram.transpose()
    return (stop_ngram(ngram_tokens, stop_threshold=stop_threshold) or
            bad_pos_gram(ngram_pos))


def _matches(ngram_tokens):
    """Helper method to determine which tokens are matches for UMLS."""
    return [token in UMLS_TERMS for token in ngram_tokens]


def exact_match(ngram_tokens):
    """Determine if all of the tokens in the gram are UMLS matches."""
    return all(_matches(ngram_tokens))


def partial_match(ngram_tokens, required_matches):
    """Determine if some of the tokens in the gram are UMLS matches.

    Determine if there are at least as many UMLS matches as specified by
    the required_matches argument.
    """
    return _matches(ngram_tokens).count(True) >= required_matches


def any_match(ngram_tokens):
    """Determine if any of the tokens in the gram are UMLS matches."""
    return any(_matches(ngram_tokens))


def valid_ngram(ngram, strictness="exact"):
    """Perform validation on the gram.

    Checks if:
        - the gram contains unique words.
        - the gram is a unigram only if it is a certain POS.
        - the gram does not violate POS restrictions.
        - the gram does not violate stop word restrictions.
        - the gram has an appropriate number of UMLS matches, based on.
          validation by the UMLS match subroutine specified by the
          strictness argument.
    """
    if len(np.unique(ngram[:, 0])) != len(ngram):
        return False

    if len(ngram) == 1 and ngram[0][1] != "NN":
        return False

    if junk_gram(ngram, stop_threshold=4):
        return False

    ngram_tokens = ngram[:, 0]
    if strictness == "exact":
        return exact_match(ngram_tokens)
    elif strictness == "any":
        return any_match(ngram_tokens)
    elif isinstance(strictness, int):
        return partial_match(ngram_tokens, strictness)

    err_msg = "strictness must be either 'exact', 'any', or an int literal"
    raise ValueError(err_msg)


def index_ngrams(ngrams, strictness='exact'):
    """Return only the valid grams."""
    ngrams = [stop_surround(ngram) for ngram in ngrams]
    return [' '.join(ngram[:, 0])
            for ngram in ngrams if valid_ngram(ngram, strictness=strictness)]


def process_bullet_n(bullet, n):
    """Process a bullet into tags.

    Given a bullet as a unicode string, run the appropriate subroutines
    to process the text into tags.
    """
    # Deal with unicode characters that don't translate into ASCII
    bullet = bullet.encode('ascii', 'replace').replace('.?', '. ').lower()
    # Get the sentences out of the bullet.
    sentences = nltk.sent_tokenize(bullet)
    valid_ngrams = set()
    for sentence in sentences:
        tagged_sentence = nltk.pos_tag(nltk.word_tokenize(sentence)[:-1])
        tagged_ngrams = nltk.ngrams(tagged_sentence, n)
        # Convert into a numpy array for simple separation of POS and Tokens.
        np_ngrams = np.array(list(tagged_ngrams))
        # "Add" the new valid ngrams to the ngrams set.
        valid_ngrams = valid_ngrams | set(index_ngrams(np_ngrams,
                                                       strictness=(n / 2)))

    return substringSieve([ngram for ngram in valid_ngrams])


def process_bullets(section):
    """Subroutine to process the bullets of a single section."""
    try:
        return set.union(*[set(process_bullet_n(bullet, n))
                           for n in range(4, 1, -1)
                           for bullet in section])
    except TypeError:
        # Sometimes sets don't play nice and set.union gets mad.
        # We except and return an empty set instead.
        return set()


def process_topic(topic):
    """Bulid the dictionary of ngrams.

    Perform final processing to build the dictionary of lists that constitute
    the extracted ngrams for the given topic.
    """
    ngrams = {k.encode('ascii', 'ignore'):
              list(set(substringSieve(list(process_bullets(v)))))
              for k, v in topic.items()}
    return ngrams


if __name__ == '__main__':
    empty_topics = []
    from string import ascii_uppercase
    for letter in ascii_uppercase:
        print(letter)
        # Open the JSON data file for reading.
        with open('JSON/mediline_{}.json'.format(letter)) as json_file:
            data = json.load(json_file)
            for key, val in data.items():
                # Parse out the tags.
                new_data = process_topic(val)
                # Keep track of which topics have no tags for the Symptoms
                # or Exams and Test sections.
                if (len(new_data['Symptoms']) == 0 or
                    len(new_data['Exams and Tests']) == 0):
                    empty_topics.append(key.encode('ascii', 'ignore'))
                # We don't need the old dictionary so we can overwrite
                # its values to lower space complexity.
                data[key] = new_data
            # Open a new JSON file for storing the tags.
            with open('JSON/parsed/mediline_{}.json'.format(letter),
                      'w') as parsed_data_file:
                json.dump(data, parsed_data_file)
    # Write the empty topics list to a file.
    with open('JSON/parsed/empty_topics.txt', 'w') as empty_topics_file:
        for topic in empty_topics:
            empty_topics_file.write('{}\n'.format(topic))
